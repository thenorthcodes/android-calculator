package com.android.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import java.util.Stack;

public class Calculator extends AppCompatActivity implements View.OnClickListener {

    SlidingDrawer slidingdrawer;
    TextView user_input;
    Button button_sin, button_cos, button_tan, button_root;
    AppCompatButton btn_1, btn_2,btn_3,btn_4,btn_5,btn_6,btn_7,btn_8,btn_9,btn_0,btn_add,btn_sub,btn_mul,btn_div,btn_dot,btn_equal, btn_del ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        initializeViews();
        initializeListeners();
    }

    public void initializeViews(){
        btn_0 = (AppCompatButton) findViewById(R.id.btn_0);
        btn_1 = (AppCompatButton) findViewById(R.id.btn_1);
        btn_2 = (AppCompatButton) findViewById(R.id.btn_2);
        btn_3 = (AppCompatButton) findViewById(R.id.btn_3);
        btn_4 = (AppCompatButton) findViewById(R.id.btn_4);
        btn_5 = (AppCompatButton) findViewById(R.id.btn_5);
        btn_6 = (AppCompatButton) findViewById(R.id.btn_6);
        btn_7 = (AppCompatButton) findViewById(R.id.btn_7);
        btn_8 = (AppCompatButton) findViewById(R.id.btn_8);
        btn_9 = (AppCompatButton) findViewById(R.id.btn_9);
        btn_add = (AppCompatButton) findViewById(R.id.btn_add);
        btn_sub = (AppCompatButton) findViewById(R.id.btn_sub);
        btn_mul = (AppCompatButton) findViewById(R.id.btn_mul);
        btn_div = (AppCompatButton) findViewById(R.id.btn_div);
        btn_equal = (AppCompatButton) findViewById(R.id.btn_equal);
        btn_dot = (AppCompatButton) findViewById(R.id.btn_dot);
        btn_del = (AppCompatButton) findViewById(R.id.btn_del);
        user_input = (TextView) findViewById(R.id.user_input);
        slidingdrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
        button_sin = (Button) findViewById(R.id.button_sin);
        button_cos = (Button) findViewById(R.id.button_cos);
        button_tan = (Button) findViewById(R.id.button_tan);
        button_root = (Button) findViewById(R.id.button_root);
		/*
		 * button_squared_2 = (Button) findViewById(R.id.button_squared_2);
		 *
		 * button_del = (Button) findViewById(R.id.button_del);
		 * button_dec = (Button) findViewById(R.id.button_dec); button_bin = (Button)
		 * findViewById(R.id.button_bin);
		 */

    }

    public void initializeListeners(){
        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_sub.setOnClickListener(this);
        btn_mul.setOnClickListener(this);
        btn_div.setOnClickListener(this);
        btn_equal.setOnClickListener(this);
        btn_dot.setOnClickListener(this);
        btn_del.setOnClickListener(this);
        btn_del.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                user_input.setText("");
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_0:
                user_input.append("0");
                break;
            case R.id.btn_1:
                user_input.append("1");
                break;
            case R.id.btn_2:
                user_input.append("2");
                break;
            case R.id.btn_3:
                user_input.append("3");
                break;
            case R.id.btn_4:
                user_input.append("4");
                break;
            case R.id.btn_5:
                user_input.append("5");
                break;
            case R.id.btn_6:
                user_input.append("6");
                break;
            case R.id.btn_7:
                user_input.append("7");
                break;
            case R.id.btn_8:
                user_input.append("8");
                break;
            case R.id.btn_9:
                user_input.append("9");
                break;
            case R.id.btn_add:
                user_input.append("+");
                btn_dot.setEnabled(true);
                break;
            case R.id.btn_sub:
                user_input.append("-");
                btn_dot.setEnabled(true);
                break;
            case R.id.btn_mul:
                user_input.append("*");
                btn_dot.setEnabled(true);
                break;
            case R.id.btn_div:
                user_input.append("÷");
                btn_dot.setEnabled(true);
                break;
            case R.id.btn_equal:
                btn_dot.setEnabled(true);
                calc();
                break;
            case R.id.btn_dot:
                user_input.append(".");
                btn_dot.setEnabled(false);
                break;
            case R.id.btn_del:
                if (user_input.getText().toString().trim().length()>0)
                user_input.setText(user_input.getText().toString().substring(0, user_input.getText().toString().length()-1));
                break;
        }
    }

    public void calc() {
        CharSequence exp = user_input.getText();
        Stack<Character> operator = new Stack<Character>();
        Stack<String> number = new Stack<String>();

        for( int i = 0; i<exp.length(); i++ ) {
            String sNumber = "";
            char sOperator = '$';

            int j = i;
            while(j < exp.length() && (exp.charAt(j) >= '0' && exp.charAt(j) <= '9' || exp.charAt(j) == '.')) {
                sNumber += exp.charAt(j);
                j++;
            }
            if( sNumber.equals("") )
                sOperator = exp.charAt(i);

            i = j>i?j-1:j;
            if(sOperator != '$') {
                if(!operator.empty()) {
                    char op = operator.peek();
                    if(getPrecedence(op)>getPrecedence(sOperator)) {
                        while(!operator.empty() && getPrecedence(op)>getPrecedence(sOperator)) {
                            evaluateExpression(number,operator);
                            if(!operator.empty())
                                op = operator.peek();
                        }
                        operator.push(sOperator);
                    }
                    else operator.push(sOperator);
                }
                else operator.push(sOperator);
            }
            else
                number.push(sNumber);
        }

        if(!operator.empty()) {
            while(!operator.empty())
                evaluateExpression(number,operator);
        }
        user_input.setText(number.pop());
    }

    public void evaluateExpression( Stack<String> number, Stack<Character> operator) {
        double num2 = Double.parseDouble(number.pop());
        double num1 = Double.parseDouble(number.pop());

        double res = 0;

        char op = operator.pop();
        switch( op ) {
            case '+':
                res = num1 + num2;
                break;
            case '-':
                res = num1 - num2;
                break;
            case '*':
                res = num1 * num2;
                break;
            case '÷':
                res = num1 / num2;
                break;

        }

        number.push(Double.toString(res));
    }

    public int getPrecedence(char operator) {

        switch(operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '÷':
                return 2;

        }
        // It must not be reached for Version 1.0
        return 0;
    }
}
